Title: Django dễ hơn Flask
Date: 2020-03-16
Category: Trang chủ
Tags: python, django, flask
Slug: first
Authors: A PyMIer
Summary: Flask là một micro-framework, thường được xem như đơn giản hơn Django, nhưng nói vậy mà không phải vậy.

## Bài gốc
https://pp.pymi.vn/article/djangovsflask/

## Học Python tại Hà Nội, Sài Gòn tại [https://PyMI.vn](PyMI.vn)
Bài này không nói Django hoàn hảo, dùng cho mọi trường hợp. Ví dụ nếu
website của bạn không dùng database, không cần trang admin (như web của data
science) thì Flask nhanh, nhẹ hơn Django nhiều phần. Hay khi bạn biết mình đang
làm gì, biết đủ những thư viện để dùng ghép lại xịn hơn cả những gì Django
có sẵn, thì Flask là lựa chọn tuyệt hảo. Flask không phải đồ chơi, [phần
core của Uber đã từng được viết bằng
Flask](https://eng.uber.com/building-tincup-microservice-implementation/),
[Airflow](https://airflow.apache.org/) - công cụ không thể thiếu trong các hệ
thống data cũng chính là một sản phẩm dùng Flask. Vậy nên, hãy học cả 2, nhé.
Và nhớ là, Django không hề khó như bạn tưởng.
